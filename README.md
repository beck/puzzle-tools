# Puzzle Tools

## Getting Started

Install a [SAT solver][]:
```
# assuming osx
brew install minisat

# run the simple problem
cd polyomino
/polyomino.py --text-render puzzles/simple.txt
```

Should output:
```
C C # #
C C B #
# B B B
A A A A
```

[SAT solver]: https://en.wikipedia.org/wiki/Boolean_satisfiability_problem

### Dev

```
npm install watch
watch "./polyomino.py --text-render puzzles/warby.txt" puzzles/
```

## Old Docs

Forked from: https://github.com/cemulate/puzzle-tools

A collection of random puzzle-solving programs which might come in handy for puzzle hunts like the [MIT Mystery Hunt](http://web.mit.edu/puzzle/www/).

Also some puzzle authoring programs for helping write puzzles (some meaning "one or more").

Current programs include:

- `algxword` -- a tool for generating theme answers in [algebraic crosswords](http://www.crosswordfiend.com/blog/2011/04/23/algebraic-crosswords-cts-10/)
- `polyomino` -- a [polyomino](http://en.wikipedia.org/wiki/Polyomino)-fitting program
