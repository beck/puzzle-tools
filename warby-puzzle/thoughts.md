# Thoughts

From minsat:

```
Found 1 solutions

L L L I I O O M M
L @ I I O O C C M
L Q S S E C C M M
N Q S S E D D R R
N Q Q Q E D @ R R
N N N @ F P H H A
J B B K F P P H A
J B B K F P G G A
J J J K K K G G @
```

🤞 there is only one

Unique pieces:
M P D

Plugging into soduku.py:
NO FIT 😢

So there are multiple solutions!
```
N N N H E P D D I
N @ S H E P D D I
N M S H E P P P I
L M S S S Q Q C C
L M M M Q Q @ C C
L L L @ A A R J J
G G O O A A R R J
G F F O B B R K K
F F O O B B K K @
```


N N N H E P D D I
N @ S H E P D D I
N M S H E P P P I
L M S S S Q Q C C
L M M M Q Q @ C C
L L L @ A A R J J
G G O O A A R R J
G F F O B B R K K
F F O O B B K K @
