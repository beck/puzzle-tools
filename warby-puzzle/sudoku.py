#!/usr/bin/env python3
import pprint

_p = pprint.pprint


def product(xs, ys):
    return tuple(x+y for x in xs for y in ys)

ROWS = 'ABCDEFGHI'
COLS = '123456789'

DIGITS = frozenset(COLS)

SQUARES = product(ROWS, COLS)
_UNITLIST = (
    tuple(product(r, COLS) for r in ROWS)
    + tuple(product(ROWS, c) for c in COLS)
    + tuple(
        product(xs, ys)
        for xs in ('ABC', 'DEF', 'GHI')
        for ys in ('123', '456', '789')
    )
)
UNITS = {
    s: {u for u in _UNITLIST if s in u}
    for s in SQUARES
}
PEERS = {
    s: frozenset(sum(us, ())) - {s}
    for s, us in UNITS.items()
}

def print_grid(g):
    size = max(len(s) for s in g) or 1
    for i in range(0, 9):
        if i in (3, 6):
            print('+'.join(('-'*(size+1)*3,)*3))
        for j in range(0, 9):
            if j in (3, 6):
                print("|", end="")
            print(
                ''.join(sorted(g[i*9 + j]) or '.').center(size),
                end=" "
            )
        print()

def parse_grid(g):
    return tuple(
        frozenset(s if s in DIGITS else ())
        for s in (c for c in g if c not in '\n|-+ ')
        if s
    )


def solved(g):
    lookup = dict(zip(SQUARES, g))
    return all(len(s) == 1 for s in g) and all(
        frozenset().union(*(lookup[s] for s in sqs)) == DIGITS
        for sqs in _UNITLIST
    )

def test():
    assert len(SQUARES) == 81

def step(g):
    lookup = dict(zip(SQUARES, singletons(g)))
    return tuple(
        s if len(s) == 1 else DIGITS - frozenset().union(*(
        # () if len(s) == 1 else DIGITS - frozenset().union(*(
            lookup[p]
            for p in PEERS[coord]
        ))
        for coord, s in lookup.items()
    )

def singletons(g):
    return tuple(s if len(s) == 1 else () for s in g)

def iterate(g):
    prev = None
    while g != prev:
        prev, g = g, step(g)
    return g

def first(xs):
    return next((x for x in xs if x), None)

def solve(g):
    g = iterate(g)
    if solved(g):
        return g
    return first(solve(p) for p in possibilities(g))

def possibilities(g):
    _, i = min(((len(s), i) for i, s in enumerate(g) if len(s) > 1), default=(None, None))
    if i is None:
        return
    for p in g[i]:
        yield g[:i] + (frozenset([p]),) + g[i+1:]


G0 = """
4 . . |. . . |8 . 5
. 3 . |. . . |. . .
. . . |7 . . |. . .
------+------+------
. 2 . |. . . |. 6 .
. . . |. 8 . |4 . .
. . . |. 1 . |. . .
------+------+------
. . . |6 . 3 |. 7 .
5 . . |2 . . |. . .
1 . 4 |. . . |. . .
"""

G1 = """
. . . |. 8 . |. . .
. . 8 |. 7 . |. 5 2
1 5 . |2 6 . |. 9 7
------+------+------
4 3 7 |. 2 5 |. . .
. 2 . |. . . |. 3 .
. . . |4 9 . |2 7 5
------+------+------
3 4 . |. 5 8 |. 6 9
8 1 . |. 4 . |7 . .
. . . |. 3 . |. . .
"""
G1_SOLVED = """
274589316
968371452
153264897
437625981
529817634
681493275
342758169
815946723
796132548
"""

def main():
    g = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"
    g2 = """
    400000805
    030000000
    000700000
    020000060
    000080400
    000010000
    000603070
    500200000
    104000000
    """
    # 4 . . |. . . |8 . 5
    # . 3 . |. . . |. . .
    # . . . |7 . . |. . .
    # ------+------+------
    # . 2 . |. . . |. 6 .
    # . . . |. 8 . |4 . .
    # . . . |. 1 . |. . .
    # ------+------+------
    # . . . |6 . 3 |. 7 .
    # 5 . . |2 . . |. . .
    # 1 . 4 |. . . |. . .
    # """
    # print_grid(parse_grid(g2))
    # print()
    # print_grid(g)
    # assert(parse_grid(g2) == parse_grid(g))
    # return
    # test()
    # _p(_UNITLIST[-9:])
    # _p(SQUARES[:9])
    # print_grid(g)
    # g1 = list(g)
    # g1[0] = DIGITS
    # print_grid(g1)
    # _p()
    # assert(solved(parse_grid(G1_SOLVED)))
    # g1 = parse_grid(G1)
    # print_grid(solve(g1))
    # assert(all(singletons((g1))))
    # print_grid(singletons(g1))
    # print_grid(singletons(g1))
    # print_grid((iterate(g1)))

    # print_grid(solve(parse_grid(G0)))
    # print_grid(solve(parse_grid('003020600900305001001806400008102900700000008006708200002609500800203009005010300')))
    # print_grid(solve(parse_grid('85...24..72......9..4.........1.7..23.5...9...4...........8..7..17..........36.4.')))
    # print_grid(solve(parse_grid('..53.....8......2..7..1.5..4....53...1..7...6..32...8..6.5....9..4....3......97..')))
    # print_grid(solve(parse_grid('000000010400000000020000000000050407008000300001090000300400200050100000000806000')))
    # print_grid(solve(parse_grid('060000300400700000000000080000008012500600000000000050082000700000500600000010000')))


# N N N H E P D D I
# N @ S H E P D D I
# N M S H E P P P I
# L M S S S Q Q C C
# L M M M Q Q @ C C
# L L L @ A A x J J
# G G x x A A x x J
# G F F x B B x K K
# F F x x B B K K @
#
# WOMP:
# . . . |. . . |. . .
# . 1 . |. . . |. . .
# . . . |. . . |. . .
# ------+------+------
# . . . |. . . |. . .
# . . . |. . . |1 . .
# . . . |1 . . |5 . .
# ------+------+------
# . . 4 |7 . . |7 6 .
# . . . |6 . . |1 . .
# . . 1 |5 . . |. . 1
#
# default:
# . . . |. . . |. . .
# . 1 . |. . . |. . .
# . . . |. . . |. . .
# ------+------+------
# . . . |. . . |. . .
# . . . |. . . |1 . .
# . . . |1 . . |. . .
# ------+------+------
# . . . |. . . |. . .
# . . . |. . . |. . .
# . . . |. . . |. . 1

    warby = """
    5 6 3 |. . . |. 4 7
    7 1 . |. . . |. . 6
    2 . . |. . . |. 1 5
    ------+------+------
    . . . |. . 9 |8 . .
    . . . |. . 3 |1 . .
    . . . |1 . 5 |. . .
    ------+------+------
    4 5 1 |. . 7 |6 . .
    9 7 6 |. . 1 |. . .
    8 3 2 |. . . |. . 1
    """

    print_grid(iterate(parse_grid(warby)))
    print_grid(solve(parse_grid(warby)))



if __name__ == "__main__":
    main()
