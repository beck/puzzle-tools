# Notes about solving

Started with a bit of research. Learned that that these shapes are called
Polyominos (tetris bits are called tetriminos).

Polyomino puzzles can be reduced down to a "boolean satisfiability problem"
aka SAT. There are lots of SAT solvers, using `brew search "sat solver"` I
installed `minisat`.

I found a polyomino puzzle > SAT converter:
https://github.com/cemulate/puzzle-tools

Forked it to:
https://gitlab.com/beck/puzzle-tools

And adapted it to use `minisat`. I defined the warby puzzle pieces (this
can also be found on gitlab) and got output:

L L L I I O O M M
L @ I I O O C C M
L Q S S E C C M M
N Q S S E D D R R
N Q Q Q E D @ R R
N N N @ F P H H A
J B B K F P P H A
J B B K F P G G A
J J J K K K G G @

After chugging in pieces that fit, discovered that this solution
failed to satisfy sudoku.

That's when I got out some paper & scissors and cut out the pieces and
started solving by hand.

Some intuition:
* no islands
* top must include a 1, so pieces: C or R (going with C since that can be swapped in for I)
* left must include a 1, so I or P

Starting with the biggest unique piece, M and working around it:
* M ok. Swap in G. Swap in F. OH! Right side looks good!

The only pieces that can fit above top-left one: L

Working through what fits, top half is done.

Working on lower-left now. Revisiting SAT solution.

Swap in N for J. Swap in R for B.

Not many pieces left, trial & error & done!

Looking at the SAT solution, here's the score:

✅ ✅ ✅ ✅ ✅ ❌ ❌ ✅ ✅
✅ 🔵 ✅ ✅ ❌ ❌ ✅ ✅ ✅
✅ ❌ ❌ ❌ ❌ ✅ ✅ ✅ ✅
❌ ❌ ❌ ❌ ❌ ❌ ❌ ✅ ✅
❌ ❌ ❌ ❌ ❌ ❌ 🔵 ✅ ✅
❌ ❌ ❌ 🔵 ✅ ✅ ✅ ✅ ✅
✅ ✅ ✅ ✅ ✅ ✅ ✅ ✅ ✅
✅ ✅ ✅ ✅ ✅ ✅ ✅ ✅ ✅
✅ ✅ ✅ ✅ ✅ ✅ ✅ ✅ 🔵

👍
